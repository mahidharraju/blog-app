import React from "react";
import { storiesOf } from "@storybook/react";
import Body from "../../../../components/Organisms/Body";
import BlogDataProvider from "../../../../context/DynamicContent/BlogDatatProvider";
import StoryRouter from "storybook-react-router";
import BlogStaticDataContextProvider from "../../../../context/BlogStaticDataContext";

storiesOf("Body", module)
  .addDecorator(StoryRouter())
  .add("Default", () => (
    <BlogDataProvider>
      <BlogStaticDataContextProvider>
        <Body />
      </BlogStaticDataContextProvider>
    </BlogDataProvider>
  ));
