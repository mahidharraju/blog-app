import React from "react";
import { storiesOf } from "@storybook/react";
import Header from "../../../../../src/components/Organisms/Header";
import StoryRouter from "storybook-react-router";

storiesOf("Header", module)
  .addDecorator(StoryRouter())
  .add("Default", () => <Header />);
