import React from "react";
import { storiesOf } from "@storybook/react";
import NewPost from "../../../../components/Organisms/NewPost";
import BlogDataProvider from "../../../../context/DynamicContent/BlogDatatProvider";

storiesOf("NewPost", module).add("Default", () => (
  <BlogDataProvider>
    <NewPost />
  </BlogDataProvider>
));
