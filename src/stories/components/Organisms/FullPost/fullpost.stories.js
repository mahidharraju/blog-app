import React from "react";
import { storiesOf } from "@storybook/react";
import Fullpost from "../../../../components/Organisms/FullPost";
import BlogDataProvider from "../../../../context/DynamicContent/BlogDatatProvider";

storiesOf("FullPost", module).add("Default", () => (
  <BlogDataProvider>
    <Fullpost postId="1" />
  </BlogDataProvider>
));
