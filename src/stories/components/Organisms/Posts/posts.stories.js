import React from "react";
import { storiesOf } from "@storybook/react";
import Posts from "../../../../components/Organisms/Posts";
import BlogDataProvider from "../../../../context/DynamicContent/BlogDatatProvider";
import StoryRouter from "storybook-react-router";

storiesOf("Posts", module)
  .addDecorator(StoryRouter())
  .add("Default", () => (
    <BlogDataProvider>
      <Posts data={data} />
    </BlogDataProvider>
  ));

const data = [
  {
    id: 1,
    title: "Title 1",
    author: "Colby Fayock",
    image:
      "https://images.unsplash.com/photo-1490544270497-a6044db8db90?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    postedOn: "Dec 6 2009",
    content:
      "Hi, it's RobHi, it's Robin. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :) engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)in. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)",
    likesCount: 244,
    disLikesCount: 992,
  },
  {
    id: 2,
    title: "Title 2",
    author: "Michael Jackson",
    image:
      "https://images.unsplash.com/photo-1523712999610-f77fbcfc3843?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
    postedOn: "Dec 6 2009",
    likesCount: 0,
    disLikesCount: 1,
    content:
      "Hi, it's Robin. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)",
  },
  {
    id: 3,
    title: "Title 3",
    author: "Robert Esdee",
    image:
      "https://images.unsplash.com/photo-1518986762393-2a8362ee6fb9?ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60",
    postedOn: "Dec 6 2009",
    likesCount: 0,
    disLikesCount: 1,
    content:
      "Hi, it's Robin. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)",
  },
  {
    id: 4,
    title: "Title 4",
    author: "Brad Westfall",
    image:
      "https://images.unsplash.com/photo-1539947257641-0f0e9f213528?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjE3MzYxfQ&auto=format&fit=crop&w=1350&q=80",
    postedOn: "Dec 6 2009",
    likesCount: 0,
    disLikesCount: 1,
    content:
      "Hi, it's Robin. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)",
  },
  {
    id: 5,
    title: "Title 5",
    author: "Ryan Florence",
    image:
      "https://images.unsplash.com/photo-1539946309076-4daf2ea73899?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
    postedOn: "Dec 6 2009",
    likesCount: 0,
    disLikesCount: 1,
    content:
      "Hi, it's Robin. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)",
  },
];
