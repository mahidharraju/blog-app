import React from "react";
import { storiesOf } from "@storybook/react";
import MainMenu from "../../../../components/Molecules/MainMenu";
import { BrowserRouter } from "react-router-dom";

storiesOf("MainMenu", module).add("Default", () => (
  <BrowserRouter>
    <MainMenu />{" "}
  </BrowserRouter>
));
