import React from "react";
import Button from "../../../../components/Atoms/Button";

export const simpleButton = () => <Button>Simple Button</Button>;
export const buttonWithAction = () => (
  <Button action={() => alert("action")}>Button With Action</Button>
);
export const linkButton = () => <Button href="/path">Link Button</Button>;

export default {
  component: Button,
  title: "Button",
};
