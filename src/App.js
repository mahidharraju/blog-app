import React, { Component } from "react";
import Page from "./components/Page/HomePage";
import BlogStaticDataContextProvider from "./context/BlogStaticDataContext";
import { BrowserRouter } from "react-router-dom";
import BlogDataProvider from "./context/DynamicContent/BlogDatatProvider";
import { ThemeProvider } from "@material-ui/core";
import Theme from "./components/Atoms/Theme";

class App extends Component {
  render() {
    return (
      <BlogStaticDataContextProvider>
        <BlogDataProvider>
          <div>
            <Page></Page>
          </div>
        </BlogDataProvider>
      </BlogStaticDataContextProvider>
    );
  }
}

export default App;
