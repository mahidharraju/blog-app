import React from "react";
import ReactDom from "react-dom";
import App from "./App";
import { ThemeProvider } from "@material-ui/core/styles";
import Theme from "./components/Atoms/Theme";
import { Auth0Provider } from "@auth0/auth0-react";
import config from "./auth_config.json";
import history from "./utils/history";

const onRedirectCallback = (appState) => {
  history.push(
    appState && appState.returnTo ? appState.returnTo : window.location.pathname
  );
};

ReactDom.render(
  <ThemeProvider theme={Theme}>
    <Auth0Provider
      domain={config.domain}
      clientId={config.clientId}
      redirectUri={window.location.origin}
      onRedirectCallback={onRedirectCallback}
    >
      <App />
    </Auth0Provider>
  </ThemeProvider>,
  document.getElementById("root")
);
