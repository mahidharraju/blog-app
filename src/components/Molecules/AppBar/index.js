import React, { useContext } from "react";
import Paper from "@material-ui/core/Paper";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import { Link } from "react-router-dom";

const AppBar = ({ roles }) => {
  const [value, setValue] = React.useState(1);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Paper square>
      <Tabs
        value={value}
        indicatorColor="primary"
        textColor="primary"
        onChange={handleChange}
        aria-label="disabled tabs example"
      >
        <Tab label={<Link to="/">About Author</Link>}></Tab>
        <Tab label={<Link to="/posts">Posts</Link>}></Tab>
        {roles.includes("ADMIN") && (
          <Tab label={<Link to="/general">New Post</Link>}></Tab>
        )}
      </Tabs>
    </Paper>
  );
};

export default AppBar;
