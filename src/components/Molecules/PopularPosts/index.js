import React, { useContext, useEffect } from "react";
import Post from "../../Molecules/Post";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid } from "@material-ui/core";
import BlogDataContext from "../../../context/DynamicContent/BlogDataContext";
import { BlogStaticDataContext } from "../../../context/BlogStaticDataContext";

const useStyles = makeStyles((theme) => ({
  postsContainer: {
    padding: theme.spacing(3),
  },
  popularTitle: {
    position: "relative",
    fontFamily: "Roboto",
    fontWeight: 500,
    textTransform: "uppercase",
    fontSize: "1.125rem",
    margin: "0 0 0.8333333333em 0",
    padding: "0 0 0.5555555556em 0.6111111111em",
    color: "#212224",
  },
}));

const PopularPosts = ({ data }) => {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className={classes.postsContainer}>
      <h3 className={classes.popularTitle}>Popular...</h3>
      <Grid container spacing={3}>
        {data.map((post, index) => (
          <Grid key={index} item xs={12}>
            <Post key={index} post={post}></Post>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default PopularPosts;
