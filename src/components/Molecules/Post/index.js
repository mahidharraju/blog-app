import React from "react";
import FavoriteOutlinedIcon from "@material-ui/icons/FavoriteOutlined";
import {
  Container,
  Grid,
  Card,
  CardActionArea,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
  CardHeader,
  Avatar,
  Box,
  IconButton,
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import { makeStyles } from "@material-ui/core/styles";
import { useLocation, Link } from "react-router-dom";
const useStyles = makeStyles((theme) => ({
  postsContainer: {
    padding: theme.spacing(3),
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
  author: {
    display: "flex",
  },
}));

const Post = ({ post }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <Link to={`/post/${post.postId}`} style={{ textDecoration: "none" }}>
        <CardMedia
          className={classes.media}
          image={post.image}
          title={post.title}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {post.title}
          </Typography>
          <Typography variant="body2" color="secondary" component="p">
            <span
              style={{
                textOverflow: "ellipsis",
                overflow: "hidden",
                width: "50px",
              }}
            >
              {post.content && post.content.length > 200
                ? post.content.substring(0, 200) + " more..."
                : post.content}
            </span>
          </Typography>
        </CardContent>
      </Link>
      <CardActions>
        <Box className={classes.author}>
          <Avatar src={post.image}></Avatar>
          <Box ml={2}>
            <Typography variant="subtitle1" component="p">
              {post.author}
            </Typography>
            <Typography variant="subtitle2" component="p">
              {post.postedOn}
            </Typography>
          </Box>
        </Box>
      </CardActions>
      <CardActions>
        <IconButton>
          <FavoriteOutlinedIcon />
          <Typography variant="subtitle2" component="p">
            {post.likesCount}
          </Typography>
        </IconButton>
        <Link to={`/post/${post.postId}`} style={{ textDecoration: "none" }}>
          <Button size="small">Full Post</Button>
        </Link>
      </CardActions>
    </Card>
  );
};

export default Post;
