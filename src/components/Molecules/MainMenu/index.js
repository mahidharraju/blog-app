import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { NavLink } from "react-router-dom";
import { useAuth0 } from "@auth0/auth0-react";
import { blogAppContext } from "../../../context/DynamicContent/BlogDataContext";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  title: {
    flexGrow: 1,
    color: "secondary",
  },
  appBar: {
    backgroundColor: "secondary",
  },
}));

const MainMenu = () => {
  const classes = useStyles();
  const {
    user,
    isAuthenticated,
    loginWithRedirect,
    logout,
    getIdTokenClaims,
    handleRedirectCallback,
  } = useAuth0();
  const { userRoles, getUserRoles, saveUser } = blogAppContext();

  const logoutWithRedirect = () =>
    logout({
      returnTo: window.location.origin,
    });

  const handleLogin = async () => {
    loginWithRedirect();
    await auth0.handleRedirectCallback();
  };

  useEffect(() => {
    if (user) {
      console.log(user);
      //saveUser(user);
      const resp = getIdTokenClaims();
      console.log(resp);
      getUserRoles(user.sub);
    } else {
    }
  }, []);

  return (
    <div className={classes.root}>
      <AppBar className={classes.appBar} position="static">
        <Toolbar>
          <Typography variant="h6" className={classes.title}>
            <NavLink to="/" exact>
              REACT BLOG
            </NavLink>
          </Typography>
          {!isAuthenticated && (
            <div>
              <Button
                data-testid="button"
                color="inherit"
                onClick={() => handleLogin()}
              >
                LogIn
              </Button>
              <Button
                data-testid="button"
                color="inherit"
                onClick={() => handleCustomLogin()}
              >
                Custom LogIn
              </Button>
            </div>
          )}
          {isAuthenticated && (
            <div>
              <Button
                data-testid="button"
                color="inherit"
                onClick={() => logoutWithRedirect()}
              >
                Log out
              </Button>
              <Typography variant="h6" className={classes.title}>
                {user.name}
              </Typography>
            </div>
          )}
          {/* {isAuthenticated && (
            <UncontrolledDropdown nav inNavbar>
              <DropdownToggle nav caret id="profileDropDown">
                <img
                  src={user.picture}
                  alt="Profile"
                  className="nav-user-profile rounded-circle"
                  width="50"
                />
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem header>{user.name}</DropdownItem>
                <DropdownItem
                  tag={RouterNavLink}
                  to="/profile"
                  className="dropdown-profile"
                  activeClassName="router-link-exact-active"
                >
                  <FontAwesomeIcon icon="user" className="mr-3" /> Profile
                </DropdownItem>
                <DropdownItem
                  id="qsLogoutBtn"
                  onClick={() => logoutWithRedirect()}
                >
                  <FontAwesomeIcon icon="power-off" className="mr-3" /> Log out
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          )} */}
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default MainMenu;
