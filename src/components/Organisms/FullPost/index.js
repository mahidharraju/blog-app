import React, { useContext, useEffect, Component } from "react";
import { blogAppContext } from "../../../context/DynamicContent/BlogDataContext";
import Container from "@material-ui/core/Container";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import ThumbDownIcon from "@material-ui/icons/ThumbDown";
import CommentTwoToneIcon from "@material-ui/icons/CommentTwoTone";
import { useAuth0 } from "@auth0/auth0-react";

import clsx from "clsx";
import {
  Typography,
  Box,
  Avatar,
  Card,
  CardMedia,
  IconButton,
  Grid,
  CardContent,
  Collapse,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  postsContainer: {
    padding: theme.spacing(3),
  },
  root: {
    maxWidth: 345,
  },
  media: {
    height: "500px",
    width: "500px",
    alignContent: "center",
  },
  author: {
    display: "flex",
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest,
    }),
  },
  expandOpen: {
    transform: "rotate(180deg)",
  },
  tewst: {
    position: "relative",
    background: "0 0",
    margin: ".5em 0 0 ",
    padding: ".5em 0 0 ",
  },
}));

const Fullpost = (props) => {
  const classes = useStyles();

  const [expanded, setExpanded] = React.useState(false);

  const handleExpandClick = () => {
    if (isAuthenticated) setExpanded(!expanded);
    else loginWithPopup();
  };

  const {
    currentBlogPost,
    getPostById,
    handleLikes,
    currentBlogPostComments,
  } = blogAppContext();

  const { isAuthenticated, loginWithRedirect, loginWithPopup } = useAuth0();

  const updateLikeCount = (id, variant) => {
    if (isAuthenticated) handleLikes(id, variant);
    else loginWithPopup();
  };

  useEffect(() => {
    getPostById(props.postId);
  }, []);

  return (
    <div>
      {currentBlogPost ? (
        <Container>
          <Grid container spacing={4}>
            <Grid item xs={12}>
              <Typography variant="h3" component="p" gutterBottom>
                {currentBlogPost.title}
              </Typography>
            </Grid>
            <Grid item xs={12}>
              <Box className={classes.author}>
                <Avatar src={currentBlogPost.image}></Avatar>
                <Box ml={2}>
                  <Typography variant="subtitle1" component="p">
                    {currentBlogPost.author}
                  </Typography>
                  <Typography variant="subtitle2" component="p">
                    {currentBlogPost.postedOn}
                  </Typography>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <CardMedia
                className={classes.media}
                image={currentBlogPost.image}
                title={currentBlogPost.title}
              />
            </Grid>
            <Grid item xs={12}>
              <Typography component="div">
                <div>{currentBlogPost.content}</div>
              </Typography>
              <Card>
                <IconButton
                  onClick={() =>
                    updateLikeCount(currentBlogPost.postId, "like")
                  }
                >
                  <ThumbUpAltIcon></ThumbUpAltIcon>
                  <Typography component="div">
                    {currentBlogPost.likesCount}
                  </Typography>
                </IconButton>
                <IconButton
                  onClick={() =>
                    updateLikeCount(currentBlogPost.postId, "dis-like")
                  }
                >
                  <ThumbDownIcon />
                  <Typography component="div">
                    {currentBlogPost.disLikesCount}
                  </Typography>
                </IconButton>
                <IconButton></IconButton>
                <IconButton
                  className={clsx(classes.expand, {
                    [classes.expandOpen]: expanded,
                  })}
                  onClick={handleExpandClick}
                  aria-expanded={expanded}
                  aria-label="show more"
                >
                  <CommentTwoToneIcon />
                  <ExpandMoreIcon></ExpandMoreIcon>
                </IconButton>
                <Collapse in={expanded} timeout="auto" unmountOnExit>
                  <CardContent>
                    <Typography paragraph>
                      <div>
                        <h3 class="ui dividing header">Comments</h3>

                        {currentBlogPostComments.map((comment) => (
                          <div className={classes.tewst}>
                            <Avatar src={currentBlogPost.image}></Avatar>
                            <div class="content">
                              <a class="author">{comment.commentedBy}</a>
                              <div class="metadata">
                                <span class="date">
                                  2020-08-06T12:32:28.310563
                                </span>
                                <div class="text"> {comment.comment}</div>
                                <div class="actions">
                                  <a class="reply">Reply</a>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </Typography>
                  </CardContent>
                </Collapse>
              </Card>
            </Grid>
          </Grid>
        </Container>
      ) : (
        <div>Loading....</div>
      )}
    </div>
  );
};

export default Fullpost;
