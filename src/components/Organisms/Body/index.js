import React, { useContext, useEffect } from "react";
import { Switch, Route } from "react-router-dom";
import AboutUs from "../AboutUs";
import AppBar from "../../Molecules/AppBar";
import Posts from "../Posts";
import { blogAppContext } from "../../../context/DynamicContent/BlogDataContext";
import { Grid } from "@material-ui/core";
import PopularPosts from "../../Molecules/PopularPosts";
import NewPost from "../NewPost";

const Body = () => {
  const { blogPosts, getPosts } = blogAppContext();
  useEffect(() => {
    getPosts();
  }, []);

  const { userRoles, getUserRoles } = blogAppContext();

  return (
    <div>
      <Grid container spacing={3} style={{ cbackgroundColorolo: "#D8D4D2" }}>
        <Grid item xs={9}>
          <AppBar roles={userRoles}></AppBar>

          <Switch>
            <Route path="/" exact component={AboutUs}></Route>
            <Route
              path="/posts"
              exact
              component={() => <Posts data={blogPosts} />}
            ></Route>
            <Route path="/general" component={() => <NewPost />}></Route>
          </Switch>
        </Grid>

        <Grid item xs={3}>
          <Route
            path="/"
            component={() => <PopularPosts data={blogPosts} />}
          ></Route>
        </Grid>
      </Grid>
    </div>
  );
};

export default Body;
