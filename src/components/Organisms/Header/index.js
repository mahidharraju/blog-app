import React, { useContext } from "react";
import Logo from "../../Atoms/Logo";
import MainMenu from "../../Molecules/MainMenu";

const Header = () => {
  return (
    <div>
      <MainMenu />
      <Logo src="NeedTotest"></Logo>
    </div>
  );
};

export default Header;
