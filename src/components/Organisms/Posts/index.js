import React, { useContext, useEffect } from "react";
import Post from "../../Molecules/Post";
import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  postsContainer: {
    padding: theme.spacing(3),
  },
}));

const Posts = ({ data }) => {
  const classes = useStyles();

  return (
    <Container maxWidth="lg" className={classes.postsContainer}>
      <Grid container spacing={3}>
        {data.map((post, index) => (
          <Grid key={index} item xs={12} sm={6} md={4}>
            <Post key={index} post={post}></Post>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default Posts;
