import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { useForm, Controller } from "react-hook-form";
import { useHistory } from "react-router-dom";
import {
  FormControl,
  TextField,
  Button,
  TextareaAutosize,
  Box,
  Grid,
} from "@material-ui/core";
import BlogDataContext from "../../../context/DynamicContent/BlogDataContext";
import { Redirect } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  postsContainer: {
    margin: "0px 50px 10px 100px",
    padding: "10px",
    backgroundColor: "primary",
  },
  input: {
    display: "none",
  },
}));

const NewPost = () => {
  const { addPost } = useContext(BlogDataContext);

  const history = useHistory();

  const { handleSubmit, register, errors, control } = useForm();

  const onSubmit = (values) => {
    addPost(values);
    console.log();
    history.push("/posts");
  };

  const classes = useStyles();

  return (
    <Grid container spacing={4}>
      <FormControl className={classes.postsContainer}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <Grid item xs={12}>
            <h3 color="primary"> New Post....</h3>
          </Grid>
          <Grid item xs={12}>
            <TextField
              name="title"
              inputRef={register}
              id="standard-full-width"
              label="Post Title"
              placeholder="Title here...."
              helperText="Write some interesting titile!"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
            <TextField
              name="imageUrl"
              inputRef={register}
              id="standard-full-width"
              label="Post Image"
              placeholder="Image Url...."
              helperText="Copy URL of yor post Image!"
              fullWidth
              margin="normal"
              InputLabelProps={{
                shrink: true,
              }}
            />
          </Grid>
          <Grid item xs={12}>
            <Controller
              control={control}
              as={TextareaAutosize}
              name="content"
              label="Here comes main content"
              aria-label="minimum height"
              rowsMin={5}
              placeholder="actual blog content here....!!!!!123"
            ></Controller>
          </Grid>
          <Grid item xs={12}>
            <Button type="submit" variant="contained" color="primary">
              Post
            </Button>
          </Grid>
        </form>
      </FormControl>
    </Grid>
  );
};

export default NewPost;
