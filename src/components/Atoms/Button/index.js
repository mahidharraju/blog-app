import React from "react";
import { node, fun, string } from "prop-types";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  buttonStyles: {
    padding: "10px",
  },
}));

const Button = ({ children, action, href }) => {
  const classes = useStyles();

  if (!href)
    return (
      <button className={classes.buttonStyles} onClick={action}>
        {children}
      </button>
    );
  return <a href={href}>{children}</a>;
};

Button.PropTypes = {
  children: node.isRequired,
  action: fun,
  href: string,
};
export default Button;
