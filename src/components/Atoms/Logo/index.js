import React from "react";
import { Box } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  logo: {
    backgroundImage: `linear-gradient(rgba(0,0,0, 0.5),rgba(0,0,0,0.5)),url('https://t3.ftcdn.net/jpg/03/19/05/40/240_F_319054056_dElukKMgLPudXiMHiNoxJcnooGopsO0L.jpg')`,
    height: "200px",
    backgroundPosition: "center",
    width: "100%",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  },
  logoText: {
    fontSize: "50px",
    margin: "0% 20% 10% 40%",
    color: "primary",
  },
}));

const Logo = ({ src }) => {
  const classes = useStyles();

  return <Box className={classes.logo}></Box>;
};

export default Logo;
