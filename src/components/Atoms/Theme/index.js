import { createMuiTheme } from "@material-ui/core/styles";
const Theme = createMuiTheme({
  palette: {
    primary: {
      main: "#F5F5F5",
    },
    secondary: {
      main: "#696969",
    },
  },
});
export default Theme;
