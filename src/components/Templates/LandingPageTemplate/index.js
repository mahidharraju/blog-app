import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary,
  },
}));

const LandingPageTemplate = (props) => {
  const classes = useStyles();
  return (
    <div>
      <Grid container spacing={3}>
        <Grid item xs={12}>
          {props.header}
        </Grid>
        <Grid item xs={12}>
          {props.body}
        </Grid>
        <Grid item xs={12}>
          {props.footer}
        </Grid>
      </Grid>
    </div>
  );
};

export default LandingPageTemplate;
