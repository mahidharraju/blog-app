import React, { useEffect } from "react";
import Header from "../../Organisms/Header";
import Body from "../../Organisms/Body";
import Footer from "../../Organisms/Footer";
import LandingPageTemplate from "../../Templates/LandingPageTemplate";
import { Route, Router } from "react-router-dom";
import Fullpost from "../../Organisms/FullPost";
import AboutUs from "../../Organisms/AboutUs";
import { AppBar } from "@material-ui/core";
import { useAuth0 } from "@auth0/auth0-react";
import history from "../../../utils/history";
const Page = () => {
  const { user, error } = useAuth0();

  if (error) {
    return <div>Oops... {error.message}</div>;
  }

  return (
    <div>
      <Router history={history}>
        <Route
          path="/"
          exact
          component={() => (
            <LandingPageTemplate
              header={<Header />}
              body={<Body />}
              footer={<Footer />}
              pageType="Home"
            />
          )}
        ></Route>
        <Route
          path="/posts"
          exact
          component={() => (
            <LandingPageTemplate
              header={<Header />}
              body={<Body />}
              footer={<Footer />}
              pageType="Home"
            />
          )}
        ></Route>
        <Route
          path="/post/:postId"
          exact
          render={({ match }) => (
            <LandingPageTemplate
              header={<Header />}
              body={<Fullpost postId={match.params.postId} />}
              footer={<Footer />}
              pageType="FullPost"
            />
          )}
        ></Route>
        <Route
          path="/general"
          exact
          component={() => (
            <LandingPageTemplate
              header={<Header />}
              body={<Body />}
              footer={<Footer />}
              pageType="General"
            />
          )}
        ></Route>
      </Router>
    </div>
  );
};

export default Page;
