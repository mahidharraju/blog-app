import React from "react";
import Body from "../components/Organisms/Body";
import { shallow, mount, render } from "enzyme";

import * as AppContext from "../context/BlogStaticDataContext";

import * as BlogAppContext from "../context/DynamicContent/BlogDataContext";
import { BrowserRouter } from "react-router-dom";

it("Body SnapShot", () => {
  const bodyNavLinks = [
    {
      id: 1,
      displayText: "About Author",
      path: "/",
      show: true,
    },
  ];
  const blogPosts = [
    {
      id: 5,
      title: "Title 5",
      author: "Ryan Florence",
      image:
        "https://images.unsplash.com/photo-1539946309076-4daf2ea73899?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80",
      postedOn: "Dec 6 2009",
      likesCount: 0,
      disLikesCount: 8,
      content:
        "Hi, it's Robin. I am a german software engineer from Berlin but working remotely most of the time. First of all, thank you for reading my blog. It's such a pleasure to have you here! Just to make sure: I am the guy on the right-hand side of the picture :)",
    },
  ];
  jest
    .spyOn(AppContext, "useAppContext")
    .mockImplementation(() => bodyNavLinks);
  jest
    .spyOn(BlogAppContext, "blogAppContext")
    .mockImplementation(() => blogPosts);
  const body = shallow(<Body />);

  expect(body).toMatchSnapshot();
  const anchorsLength = body.find("Route").length;
  expect(anchorsLength).toEqual(4);
});
