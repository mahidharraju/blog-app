import React from "react";
import { shallow, mount, render } from "enzyme";
import Fullpost from "../components/Organisms/FullPost";
import BlogDataContext, * as BlogAppContext from "../context/DynamicContent/BlogDataContext";
import { cleanup } from "@testing-library/react";
import { act, renderHook } from "@testing-library/react-hooks";
import { blogAppContext } from "../context/DynamicContent/BlogDataContext";
import BlogDataProvider from "../context/DynamicContent/BlogDatatProvider";

afterEach(cleanup);

it("Full post page load", () => {
  jest.spyOn(BlogAppContext, "blogAppContext").mockImplementation(() => "");
  const fullPost = shallow(<Fullpost />);
  expect(fullPost).toMatchSnapshot();
});
it("Full post page load with empty data", () => {
  jest.spyOn(BlogAppContext, "blogAppContext").mockImplementation(() => "");
  const fullPost = shallow(<Fullpost />);
  expect(fullPost).toMatchSnapshot();
  const text = fullPost.find("div").last().text();
  expect(text).toMatch("Loading");
});
it("Full post page with Dummy Data", () => {
  const useEffect = jest
    .spyOn(React, "useEffect")
    .mockImplementation((f) => f());

  const fullPost = shallow(<Fullpost />);
  console.log(fullPost);
  expect(fullPost).toMatchSnapshot();
  expect(useEffect).toHaveBeenCalled();
});
it("Full post page with Dummy Data", () => {
  const getPostById = jest
    .spyOn(BlogDataProvider, "getPostById()")
    .mockImplementation((f) => f());

  const fullPost = mount(
    <BlogDataContext.Provider value={{ getPostById: getPostById }}>
      <Fullpost />
    </BlogDataContext.Provider>
  );
  console.log(fullPost);
  expect(fullPost).toMatchSnapshot();
  expect(getPostById).toHaveBeenCalled();
});
