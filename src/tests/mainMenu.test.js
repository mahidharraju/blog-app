import React from "react";
import { shallow, mount } from "enzyme";
import { render, cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";
import MainMenu from "../components/Molecules/MainMenu";
import "@testing-library/jest-dom/extend-expect";
import { BrowserRouter } from "react-router-dom";

afterEach(cleanup);
describe("MainMenu component", () => {
  describe("Simple Menu Test with out any arguments", () => {
    it("should have only one Div", () => {
      const mainMenu = shallow(<MainMenu />);
      // expect(mainMenu).toMatchSnapshot();
      expect(mainMenu.find("NavLink").length).toEqual(1);
    });
  });
  it("renders MainMenu", () => {
    const { getByTestId } = render(
      <BrowserRouter>
        <MainMenu></MainMenu>
      </BrowserRouter>
    );

    expect(getByTestId("button")).toHaveTextContent("Log out");
  });
  it("NavLink Content check", () => {
    const wrapper = mount(
      <BrowserRouter>
        <MainMenu></MainMenu>
      </BrowserRouter>
    );
    const headerText = wrapper.find("NavLink").text();
    const logOutText = wrapper.find("span").first().text();
    expect(wrapper).toMatchSnapshot();

    expect(logOutText).toEqual("Log out");
    expect(headerText).toEqual("REACT BLOG");
  });
});
