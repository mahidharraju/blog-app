import React, { useReducer } from "react";
import BlogDataContext from "./BlogDataContext";
import BlogDataReducer from "./BlogDataReducer";
import uuid from "uuid";
import { useAuth0 } from "@auth0/auth0-react";
import { raw } from "file-loader";

const API_ENDPOINT_URL = "http://localhost:8443/api/v1";
const BlogDataProvider = (props) => {
  const initialState = {
    blogPosts: [],
    currentBlogPost: null,
    currentBlogPostComments: [],
    loading: true,
    authors: [],
    userRoles: [],
  };

  const { getAccessTokenSilently, getIdTokenClaims } = useAuth0();

  var today = new Date();
  const date =
    today.getFullYear() + "-" + (today.getMonth() + 1) + "-" + today.getDate();

  const [state, dispatch] = useReducer(BlogDataReducer, initialState);

  const getPosts = async () => {
    try {
      dispatch({ type: "SENDING_REQUEST" });

      const respose = await fetch(API_ENDPOINT_URL + "/posts");
      const data = await respose.json();
      dispatch({ type: "REQUEST_FINISHED" });
      dispatch({ type: "SET_POSTS", payload: data });
    } catch (err) {
      console.log(err);
    }
  };

  const getPostById = async (id) => {
    try {
      dispatch({ type: "SENDING_REQUEST" });
      const respose = await fetch(API_ENDPOINT_URL + `/posts/${id}`);
      const data = await respose.json();

      dispatch({ type: "SET_POST", payload: data });
      const commentsResp = await fetch(
        API_ENDPOINT_URL + `/posts/${id}/comments`
      );
      const comments = await commentsResp.json();

      dispatch({ type: "SET_COMMENTS", payload: comments });
      dispatch({ type: "REQUEST_FINISHED" });
    } catch (err) {
      console.log(err);
    }
  };

  const getAuthors = async () => {
    try {
      dispatch({ type: "SENDING_REQUEST" });
      const respose = await fetch(`http://localhost:5000/authors`);
      const data = await respose.json();
      dispatch({ type: "REQUEST_FINISHED" });
      dispatch({ type: "SET_AUTHORS", payload: data });
    } catch (err) {
      console.log(err);
    }
  };

  const getUserRoles = async (userId) => {
    try {
      dispatch({ type: "SENDING_REQUEST" });
      console.log(getIdTokenClaims());

      const claims = await getIdTokenClaims();
      console.log("tocken" + claims.__raw);
      const token = claims.__raw;
      const respose = await fetch(
        `https://dev-6mc8ojvq.us.auth0.com/api/v2/users/${userId}/roles`,
        {
          headers: {
            authorization: `Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlpEMVcxQnRrX19sZXBabjgxcEFkRSJ9.eyJpc3MiOiJodHRwczovL2Rldi02bWM4b2p2cS51cy5hdXRoMC5jb20vIiwic3ViIjoiSFlSUTBOYVlYTTFaV0VWNGxuOXVMMFFCOXMyUlVBSzhAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vZGV2LTZtYzhvanZxLnVzLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTk3MDQ2Njg3LCJleHAiOjE1OTcxMzMwODcsImF6cCI6IkhZUlEwTmFZWE0xWldFVjRsbjl1TDBRQjlzMlJVQUs4Iiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSByZWFkOnVzZXJfY3VzdG9tX2Jsb2NrcyBjcmVhdGU6dXNlcl9jdXN0b21fYmxvY2tzIGRlbGV0ZTp1c2VyX2N1c3RvbV9ibG9ja3MgY3JlYXRlOnVzZXJfdGlja2V0cyByZWFkOmNsaWVudHMgdXBkYXRlOmNsaWVudHMgZGVsZXRlOmNsaWVudHMgY3JlYXRlOmNsaWVudHMgcmVhZDpjbGllbnRfa2V5cyB1cGRhdGU6Y2xpZW50X2tleXMgZGVsZXRlOmNsaWVudF9rZXlzIGNyZWF0ZTpjbGllbnRfa2V5cyByZWFkOmNvbm5lY3Rpb25zIHVwZGF0ZTpjb25uZWN0aW9ucyBkZWxldGU6Y29ubmVjdGlvbnMgY3JlYXRlOmNvbm5lY3Rpb25zIHJlYWQ6cmVzb3VyY2Vfc2VydmVycyB1cGRhdGU6cmVzb3VyY2Vfc2VydmVycyBkZWxldGU6cmVzb3VyY2Vfc2VydmVycyBjcmVhdGU6cmVzb3VyY2Vfc2VydmVycyByZWFkOmRldmljZV9jcmVkZW50aWFscyB1cGRhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGRlbGV0ZTpkZXZpY2VfY3JlZGVudGlhbHMgY3JlYXRlOmRldmljZV9jcmVkZW50aWFscyByZWFkOnJ1bGVzIHVwZGF0ZTpydWxlcyBkZWxldGU6cnVsZXMgY3JlYXRlOnJ1bGVzIHJlYWQ6cnVsZXNfY29uZmlncyB1cGRhdGU6cnVsZXNfY29uZmlncyBkZWxldGU6cnVsZXNfY29uZmlncyByZWFkOmhvb2tzIHVwZGF0ZTpob29rcyBkZWxldGU6aG9va3MgY3JlYXRlOmhvb2tzIHJlYWQ6YWN0aW9ucyB1cGRhdGU6YWN0aW9ucyBkZWxldGU6YWN0aW9ucyBjcmVhdGU6YWN0aW9ucyByZWFkOmVtYWlsX3Byb3ZpZGVyIHVwZGF0ZTplbWFpbF9wcm92aWRlciBkZWxldGU6ZW1haWxfcHJvdmlkZXIgY3JlYXRlOmVtYWlsX3Byb3ZpZGVyIGJsYWNrbGlzdDp0b2tlbnMgcmVhZDpzdGF0cyByZWFkOnRlbmFudF9zZXR0aW5ncyB1cGRhdGU6dGVuYW50X3NldHRpbmdzIHJlYWQ6bG9ncyByZWFkOnNoaWVsZHMgY3JlYXRlOnNoaWVsZHMgdXBkYXRlOnNoaWVsZHMgZGVsZXRlOnNoaWVsZHMgcmVhZDphbm9tYWx5X2Jsb2NrcyBkZWxldGU6YW5vbWFseV9ibG9ja3MgdXBkYXRlOnRyaWdnZXJzIHJlYWQ6dHJpZ2dlcnMgcmVhZDpncmFudHMgZGVsZXRlOmdyYW50cyByZWFkOmd1YXJkaWFuX2ZhY3RvcnMgdXBkYXRlOmd1YXJkaWFuX2ZhY3RvcnMgcmVhZDpndWFyZGlhbl9lbnJvbGxtZW50cyBkZWxldGU6Z3VhcmRpYW5fZW5yb2xsbWVudHMgY3JlYXRlOmd1YXJkaWFuX2Vucm9sbG1lbnRfdGlja2V0cyByZWFkOnVzZXJfaWRwX3Rva2VucyBjcmVhdGU6cGFzc3dvcmRzX2NoZWNraW5nX2pvYiBkZWxldGU6cGFzc3dvcmRzX2NoZWNraW5nX2pvYiByZWFkOmN1c3RvbV9kb21haW5zIGRlbGV0ZTpjdXN0b21fZG9tYWlucyBjcmVhdGU6Y3VzdG9tX2RvbWFpbnMgdXBkYXRlOmN1c3RvbV9kb21haW5zIHJlYWQ6ZW1haWxfdGVtcGxhdGVzIGNyZWF0ZTplbWFpbF90ZW1wbGF0ZXMgdXBkYXRlOmVtYWlsX3RlbXBsYXRlcyByZWFkOm1mYV9wb2xpY2llcyB1cGRhdGU6bWZhX3BvbGljaWVzIHJlYWQ6cm9sZXMgY3JlYXRlOnJvbGVzIGRlbGV0ZTpyb2xlcyB1cGRhdGU6cm9sZXMgcmVhZDpwcm9tcHRzIHVwZGF0ZTpwcm9tcHRzIHJlYWQ6YnJhbmRpbmcgdXBkYXRlOmJyYW5kaW5nIGRlbGV0ZTpicmFuZGluZyByZWFkOmxvZ19zdHJlYW1zIGNyZWF0ZTpsb2dfc3RyZWFtcyBkZWxldGU6bG9nX3N0cmVhbXMgdXBkYXRlOmxvZ19zdHJlYW1zIGNyZWF0ZTpzaWduaW5nX2tleXMgcmVhZDpzaWduaW5nX2tleXMgdXBkYXRlOnNpZ25pbmdfa2V5cyByZWFkOmxpbWl0cyB1cGRhdGU6bGltaXRzIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.V3mN5056Xuukjzp3D1PD5YPITBlM86IJWIOpkgJ1-BuWZXIqZzX7LYzi59vL_kEvszxvQFDaUb2geebPcpnpZ0XakzYBQf4y10pXfwiunkzDcFkV0IrWOX6OGTgQnAQzv13R2G1zGYB43AEqEBKP6Pk7mnZOeb0f5sSxTQoxyeJxjFosE_gWVhYhBjO01R4_FcjDT6uk50_UHFLpr2a2gneNxmaHimBz7LyUArYo13P9mbHQoBXVIcjQyxUg5Mhn725RJPwzjBAghTXK_BHTZT6W8RR1XSyfxuJ4rNBRSr8aLCJBrm2FmW6i_JNf2htr94YPqaJVVCmaqxIFlFvEaQ`,
          },
        }
      );
      const data = await respose.json();
      dispatch({ type: "REQUEST_FINISHED" });
      dispatch({ type: "SET_ROLES", payload: data.map((role) => role.name) });
    } catch (err) {
      console.log(err);
    }
  };
  const saveUser = async (user) => {
    request(
      {
        method: "POST",
        url: "http://dumy.com/api",
        headers: {
          "Content-Type": "application/json",
        },
        body: `{  "email": "${user.email}", "fkAuth": "${user.id}" }`,
      },
      function (error, response, body) {
        console.log(user);
        console.log(context);
        cb(error, response);
      }
    );
  };
  const addPost = async (data) => {
    const axios = require("axios");
    const claims = await getIdTokenClaims();
    const token = claims.__raw;
    alert(token);
    const headers = {
      "Content-Type": "application/json",
      Authorization: token,
    };
    const postData = {
      title: data.title,
      author: "4e99a87f-7478-466b-b345-f72f68faa934",
      image: data.imageUrl,
      content: data.content,
    };
    /*  try {
      dispatch({ type: "SENDING_REQUEST" });
      axios
        .post(API_ENDPOINT_URL + "/posts", postData, { headers: headers })
        .then((resp) => {
          console.log(resp.data);
        })
        .catch((error) => {
          console.log(error);
        }); */
    try {
      dispatch({ type: "SENDING_REQUEST" });
      alert();
      const respose = await fetch(API_ENDPOINT_URL + `/posts`, {
        method: post,
        headers: {
          authorization: `Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlpEMVcxQnRrX19sZXBabjgxcEFkRSJ9.eyJpc3MiOiJodHRwczovL2Rldi02bWM4b2p2cS51cy5hdXRoMC5jb20vIiwic3ViIjoiSFlSUTBOYVlYTTFaV0VWNGxuOXVMMFFCOXMyUlVBSzhAY2xpZW50cyIsImF1ZCI6Imh0dHBzOi8vZGV2LTZtYzhvanZxLnVzLmF1dGgwLmNvbS9hcGkvdjIvIiwiaWF0IjoxNTk3MDQ2Njg3LCJleHAiOjE1OTcxMzMwODcsImF6cCI6IkhZUlEwTmFZWE0xWldFVjRsbjl1TDBRQjlzMlJVQUs4Iiwic2NvcGUiOiJyZWFkOmNsaWVudF9ncmFudHMgY3JlYXRlOmNsaWVudF9ncmFudHMgZGVsZXRlOmNsaWVudF9ncmFudHMgdXBkYXRlOmNsaWVudF9ncmFudHMgcmVhZDp1c2VycyB1cGRhdGU6dXNlcnMgZGVsZXRlOnVzZXJzIGNyZWF0ZTp1c2VycyByZWFkOnVzZXJzX2FwcF9tZXRhZGF0YSB1cGRhdGU6dXNlcnNfYXBwX21ldGFkYXRhIGRlbGV0ZTp1c2Vyc19hcHBfbWV0YWRhdGEgY3JlYXRlOnVzZXJzX2FwcF9tZXRhZGF0YSByZWFkOnVzZXJfY3VzdG9tX2Jsb2NrcyBjcmVhdGU6dXNlcl9jdXN0b21fYmxvY2tzIGRlbGV0ZTp1c2VyX2N1c3RvbV9ibG9ja3MgY3JlYXRlOnVzZXJfdGlja2V0cyByZWFkOmNsaWVudHMgdXBkYXRlOmNsaWVudHMgZGVsZXRlOmNsaWVudHMgY3JlYXRlOmNsaWVudHMgcmVhZDpjbGllbnRfa2V5cyB1cGRhdGU6Y2xpZW50X2tleXMgZGVsZXRlOmNsaWVudF9rZXlzIGNyZWF0ZTpjbGllbnRfa2V5cyByZWFkOmNvbm5lY3Rpb25zIHVwZGF0ZTpjb25uZWN0aW9ucyBkZWxldGU6Y29ubmVjdGlvbnMgY3JlYXRlOmNvbm5lY3Rpb25zIHJlYWQ6cmVzb3VyY2Vfc2VydmVycyB1cGRhdGU6cmVzb3VyY2Vfc2VydmVycyBkZWxldGU6cmVzb3VyY2Vfc2VydmVycyBjcmVhdGU6cmVzb3VyY2Vfc2VydmVycyByZWFkOmRldmljZV9jcmVkZW50aWFscyB1cGRhdGU6ZGV2aWNlX2NyZWRlbnRpYWxzIGRlbGV0ZTpkZXZpY2VfY3JlZGVudGlhbHMgY3JlYXRlOmRldmljZV9jcmVkZW50aWFscyByZWFkOnJ1bGVzIHVwZGF0ZTpydWxlcyBkZWxldGU6cnVsZXMgY3JlYXRlOnJ1bGVzIHJlYWQ6cnVsZXNfY29uZmlncyB1cGRhdGU6cnVsZXNfY29uZmlncyBkZWxldGU6cnVsZXNfY29uZmlncyByZWFkOmhvb2tzIHVwZGF0ZTpob29rcyBkZWxldGU6aG9va3MgY3JlYXRlOmhvb2tzIHJlYWQ6YWN0aW9ucyB1cGRhdGU6YWN0aW9ucyBkZWxldGU6YWN0aW9ucyBjcmVhdGU6YWN0aW9ucyByZWFkOmVtYWlsX3Byb3ZpZGVyIHVwZGF0ZTplbWFpbF9wcm92aWRlciBkZWxldGU6ZW1haWxfcHJvdmlkZXIgY3JlYXRlOmVtYWlsX3Byb3ZpZGVyIGJsYWNrbGlzdDp0b2tlbnMgcmVhZDpzdGF0cyByZWFkOnRlbmFudF9zZXR0aW5ncyB1cGRhdGU6dGVuYW50X3NldHRpbmdzIHJlYWQ6bG9ncyByZWFkOnNoaWVsZHMgY3JlYXRlOnNoaWVsZHMgdXBkYXRlOnNoaWVsZHMgZGVsZXRlOnNoaWVsZHMgcmVhZDphbm9tYWx5X2Jsb2NrcyBkZWxldGU6YW5vbWFseV9ibG9ja3MgdXBkYXRlOnRyaWdnZXJzIHJlYWQ6dHJpZ2dlcnMgcmVhZDpncmFudHMgZGVsZXRlOmdyYW50cyByZWFkOmd1YXJkaWFuX2ZhY3RvcnMgdXBkYXRlOmd1YXJkaWFuX2ZhY3RvcnMgcmVhZDpndWFyZGlhbl9lbnJvbGxtZW50cyBkZWxldGU6Z3VhcmRpYW5fZW5yb2xsbWVudHMgY3JlYXRlOmd1YXJkaWFuX2Vucm9sbG1lbnRfdGlja2V0cyByZWFkOnVzZXJfaWRwX3Rva2VucyBjcmVhdGU6cGFzc3dvcmRzX2NoZWNraW5nX2pvYiBkZWxldGU6cGFzc3dvcmRzX2NoZWNraW5nX2pvYiByZWFkOmN1c3RvbV9kb21haW5zIGRlbGV0ZTpjdXN0b21fZG9tYWlucyBjcmVhdGU6Y3VzdG9tX2RvbWFpbnMgdXBkYXRlOmN1c3RvbV9kb21haW5zIHJlYWQ6ZW1haWxfdGVtcGxhdGVzIGNyZWF0ZTplbWFpbF90ZW1wbGF0ZXMgdXBkYXRlOmVtYWlsX3RlbXBsYXRlcyByZWFkOm1mYV9wb2xpY2llcyB1cGRhdGU6bWZhX3BvbGljaWVzIHJlYWQ6cm9sZXMgY3JlYXRlOnJvbGVzIGRlbGV0ZTpyb2xlcyB1cGRhdGU6cm9sZXMgcmVhZDpwcm9tcHRzIHVwZGF0ZTpwcm9tcHRzIHJlYWQ6YnJhbmRpbmcgdXBkYXRlOmJyYW5kaW5nIGRlbGV0ZTpicmFuZGluZyByZWFkOmxvZ19zdHJlYW1zIGNyZWF0ZTpsb2dfc3RyZWFtcyBkZWxldGU6bG9nX3N0cmVhbXMgdXBkYXRlOmxvZ19zdHJlYW1zIGNyZWF0ZTpzaWduaW5nX2tleXMgcmVhZDpzaWduaW5nX2tleXMgdXBkYXRlOnNpZ25pbmdfa2V5cyByZWFkOmxpbWl0cyB1cGRhdGU6bGltaXRzIiwiZ3R5IjoiY2xpZW50LWNyZWRlbnRpYWxzIn0.V3mN5056Xuukjzp3D1PD5YPITBlM86IJWIOpkgJ1-BuWZXIqZzX7LYzi59vL_kEvszxvQFDaUb2geebPcpnpZ0XakzYBQf4y10pXfwiunkzDcFkV0IrWOX6OGTgQnAQzv13R2G1zGYB43AEqEBKP6Pk7mnZOeb0f5sSxTQoxyeJxjFosE_gWVhYhBjO01R4_FcjDT6uk50_UHFLpr2a2gneNxmaHimBz7LyUArYo13P9mbHQoBXVIcjQyxUg5Mhn725RJPwzjBAghTXK_BHTZT6W8RR1XSyfxuJ4rNBRSr8aLCJBrm2FmW6i_JNf2htr94YPqaJVVCmaqxIFlFvEaQ`,
        },
        body: {
          title: data.title,
          author: "4e99a87f-7478-466b-b345-f72f68faa934",
          image: data.imageUrl,
          content: data.content,
        },
      });

      dispatch({ type: "REQUEST_FINISHED" });
    } catch (err) {
      console.log(err);
    }
  };

  const handleLikes = async (id, variant) => {
    const axios = require("axios");
    try {
      dispatch({ type: "SENDING_REQUEST" });
      if (variant === "like")
        await axios
          .patch(API_ENDPOINT_URL + `/posts/${id}/like`)
          .then((resp) => {
            dispatch({ type: "LIKE_UPGRADE", payload: resp.data });
          });
      else if (variant === "dis-like")
        await axios
          .patch(API_ENDPOINT_URL + `/posts/${id}/disLike`)
          .then((resp) => {
            dispatch({ type: "LIKE_UPGRADE", payload: resp.data });
          });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <BlogDataContext.Provider
      value={{
        blogPosts: state.blogPosts,
        currentBlogPost: state.currentBlogPost,
        loading: state.loading,
        getPosts: getPosts,
        getPostById: getPostById,
        authors: state.authors,
        getAuthors: getAuthors,
        addPost: addPost,
        handleLikes: handleLikes,
        userRoles: state.userRoles,
        getUserRoles: getUserRoles,
        currentBlogPostComments: state.currentBlogPostComments,
        saveUser: saveUser,
      }}
    >
      {props.children}
    </BlogDataContext.Provider>
  );
};

export default BlogDataProvider;
