import { createContext, useContext } from "react";

const BlogDataContext = createContext();

export const blogAppContext = () => useContext(BlogDataContext);

export default BlogDataContext;
