export default (state, action) => {
  switch (action.type) {
    case "SET_POSTS":
      return {
        ...state,
        blogPosts: action.payload,
      };
    case "SET_POST":
      return {
        ...state,
        currentBlogPost: action.payload,
      };
    case "SET_COMMENTS":
      return {
        ...state,
        currentBlogPostComments: action.payload,
      };

    case "SET_AUTHORS":
      return {
        ...state,
        authors: action.payload,
      };
    case "SET_ROLES":
      return {
        ...state,
        userRoles: action.payload,
      };
    case "SENDING_REQUEST":
      return {
        ...state,
        loading: true,
      };
    case "REQUEST_FINISHED":
      return {
        ...state,
        loading: false,
      };
    case "LIKE_UPGRADE":
      return {
        ...state,
        currentBlogPost: action.payload,
      };
  }
};
