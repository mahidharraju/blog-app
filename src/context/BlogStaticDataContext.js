import React, { createContext, useState, useContext } from "react";
import AboutUs from "../components/Organisms/AboutUs";

export const BlogStaticDataContext = createContext();

export const useAppContext = () => useContext(BlogStaticDataContext);

const BlogStaticDataContextProvider = (props) => {
  const [loggedInUser, setLoginUser] = useState([
    { userId: 1, userName: "testUser" },
  ]);

  const [isLoggedin, setLoginStatus] = useState([{ isloggedIn: true }]);

  const [leftNavLinks, setLeftNavs] = useState([
    {
      id: 1,
      displayText: "Home",
      path: "/home",
      component: AboutUs,
      show: true,
    },
    {
      id: 2,
      displayText: "About Author",
      path: "/aboutUs",
      component: AboutUs,
      show: true,
    },
    {
      id: 3,
      displayText: "Contact Us",
      path: "/contactUs",
      component: AboutUs,
      show: true,
    },
  ]);

  const [rightNavLinks, setRightNavs] = useState([
    {
      id: 1,
      displayText: "Sign-In",
      path: "/signIn",
      component: { AboutUs },
      show: true,
    },
    {
      id: 2,
      displayText: "Sign-Up",
      path: "/signUp",
      component: { AboutUs },
      show: true,
    },
    {
      id: 3,
      displayText: "Log-out",
      path: "/logOut",
      component: { AboutUs },
      show: true,
    },
  ]);

  const [bodyNavLinks, setBodyNavs] = useState([
    {
      id: 1,
      displayText: "About Author",
      path: "/",
      show: true,
    },
    {
      id: 2,
      displayText: "Posts",
      path: "/posts",
      show: true,
    },
    {
      id: 3,
      displayText: "New Post",
      path: "/general",
      show: true,
    },
  ]);

  const [headerLogo, setheaderLogo] = useState(
    "../../../assets/header_logo.png"
  );

  return (
    <BlogStaticDataContext.Provider
      value={{
        leftNavLinks,
        setLeftNavs,
        rightNavLinks,
        setRightNavs,
        headerLogo,
        bodyNavLinks,
        setBodyNavs,
      }}
    >
      {props.children}
    </BlogStaticDataContext.Provider>
  );
};

export default BlogStaticDataContextProvider;
