# pull official base image
FROM node:latest

# set working directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# add `/app/node_modules/.bin` to $PATH
ENV PATH /BLOG-APP/node_modules/.bin:$PATH

# install app dependencies
COPY package.json /usr/src/app
COPY package-lock.json /usr/src/app

RUN npm install



# add app
COPY . /usr/src/app

# start app
CMD ["npm", "run","dev"]  
