import { configure } from "@storybook/react";

configure(
  require.context("../src/stories/components", true, /\.stories\.js$/),
  module
);
